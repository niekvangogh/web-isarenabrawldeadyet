let element = $("#gamecount");

$(document).ready(function () {
    refresh();

    setInterval(function () {
        refresh();
    }, 15 * 1000);
});

function refresh() {
    loadResults(function (data) {
        if (data.success) {
            let playerCount = data.result.current_players;
            let playerRecord = data.result.player_record;
            update(playerCount, playerRecord);
        } else {
            element.text(`An error occured: ${data.error}`);
        }
    });
}

function update(playerCount, recordCount) {
    let text;
    if (playerCount === 0) {
        text = "<h2>🎉 ARENA BRAWL IS ACTUALLY DEAD! 🎉</h2>"
    } else {
        text = `<h2>🎉 ARENA BRAWL IS NOT DEAD YET! 🎉</h2><h3>The game currently has ${playerCount} ${playerCount === 1 ? "player" : "players"}! <span id='record'>(Record being ${recordCount})</span>`;
    }
    element.html(text);
}

function loadResults(cb) {
    $.getJSON({
        url: "/update",
        success: function (data) {
            if (cb) {
                cb(data);
            }
        }
    });
}