var request = require('request');
var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
    res.render('index', {title: 'Is Arena Brawl dead yet?'});
});

router.get('/update', async function (req, res, next) {
    getPlayerCounts().then(playerCounts => {
        const players = playerCounts.games.LEGACY.modes.ARENA;
        res.json({
            success: true, result: {
                current_players: players,
                player_record: 98
            }
        }).end();
    }).catch(error => {
        if (error) {
            res.json({
                success: false, reason: "An error occurred"
            }).end();
        }
    });
});

function getPlayerCounts() {
    return new Promise(function (resolve, reject) {
        request(`https://api.hypixel.net/gamecounts?key=${process.env.HYPIXEL_API_KEY}`, function (error, response, body) {
            if (error) {
                reject(error);
            } else {
                let json = JSON.parse(body);
                if (json.success) {
                    resolve(json);
                } else {
                    reject(json.cause);
                }
            }
        });
    });
}


module.exports = router;